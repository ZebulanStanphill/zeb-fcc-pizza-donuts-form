# Zeb’s freeCodeCamp Pizza Donuts Form

## Description

A pretend food delivery form created for a freeCodeCamp project.

## Licensing info

© 2018-2019 Zebulan Stanphill

This program is free (as in freedom) software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

This project loads the testable-projects-fcc script:

https://github.com/freeCodeCamp/testable-projects-fcc

© 2017 freeCodeCamp

Released under the BSD 3-Clause License:

https://github.com/freeCodeCamp/testable-projects-fcc/blob/master/LICENSE.md